output "application_eks_cluster_name" {
  value       = module.eks_cluster_tf.cluster_id
  description = "Nome do cluster EKS."
}

output "application_eks_workers_security_group_id" {
  value       = module.eks_cluster_tf.worker_security_group_id
  description = "ID do security group dos workers do cluster EKS."
}
