variable "node_group_name" {
  description = "Nome do node group."
  type        = string
}

variable "compute_ec2_manager_server_k8s_sg_id" {
  type        = string
  description = "ID do security group do manager server do k8s."
}

variable "instance_type" {
  type        = string
  description = "Tipo de instânica para os workers do EKS."
}

variable "subnet_ids" {
  type        = list(string)
  description = "ID das subnets para o EKS."

}

variable "manage_aws_auth" {
  type        = bool
  description = "Determina se o configmap aws-auth será gerenciado pelo Terraform. Só funciona se a máquina do Terraform tiver comunicação com o endpoint do cluster EKS."
}

variable "cluster_name" {
  type        = string
  description = "Nome do cluster EKS."
}


variable "key_name" {
  type        = string
  description = "Nome da key pair para os workers do EKS."
}

variable "k8s_version" {
  type        = string
  description = "Versão do Kubernetes."
}

variable "asg_max_size" {
  type        = number
  description = "Capacidade máxima para o Auto Scaling Group dos workers do EKS."
}

variable "asg_min_size" {
  type        = number
  description = "Capacidade mínima para o Auto Scaling Group dos workers do EKS."
}

variable "asg_desired_capacity" {
  type        = number
  description = "Tamanho desejado para o Auto Scaling Group dos workers do EKS."
}

variable "launch_template_version" {
  type        = string
  description = "Versão do Launch Template dos workers do EKS."
}


variable "roles_aws_auth" {

  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))
  description = "Roles do IAM para interagirem com o Kubernetes. Obs: permite a visualização na console AWS dependendo do grupo escolhido."
}

variable "cluster_endpoint_public_access" {

  type        = bool
  description = "Ativa o endpoint público do cluster EKS."
}

variable "cluster_endpoint_private_access" {

  type        = bool
  description = "Ativa o endpoint privado do cluster EKS."
}

variable "users_aws_auth" {

  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  description = "Usuários do IAM para interagirem com o Kubernetes. Obs: permite a visualização na console AWS dependendo do grupo escolhido."
}

/*
variable "cluster_additional_permissions" {
  type        = list(string)
  description = "Permissões adicionais ppara os workers do cluster EKS. Exemplo: permissões autoscaling: para o Cluster Auto Scaler."
  default     = ["autoscaling:DescribeAutoScalingGroups", "autoscaling:DescribeAutoScalingInstances", "autoscaling:DescribeLaunchConfigurations", "autoscaling:DescribeTags", "autoscaling:SetDesiredCapacity", "autoscaling:TerminateInstanceInAutoScalingGroup", "ec2:DescribeLaunchTemplateVersions"]
}
*/

variable "accounts_aws_auth" {
  type        = list(string)
  default     = []
  description = "AWS Account IDs  para interagirem com o Kubernetes. Obs: permite a visualização na console AWS dependendo do grupo escolhido."
}

variable "cluster_enabled_log_types" {
  type        = list(string)
  default     = []
  description = "Lista com os tipos de logs que serão habilitados para o cluster EKS."
}
variable "environment" {
  description = "Ambiente do projeto."
  type        = string
}

variable "monitoring" {
  type        = bool
  description = "Habilita o monitoramento detalhado para os workers do EKS."
}

variable "root_volume_size" {
  type        = number
  description = "Tamanho do volume EBS para os workers do EKS."
}

variable "root_volume_type" {
  type        = string
  description = "Tipo de volume EBS para os workers do EKS."
}
