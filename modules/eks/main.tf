data "aws_eks_cluster" "cluster" {
  name = module.eks_cluster_tf.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks_cluster_tf.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  token                  = data.aws_eks_cluster_auth.cluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
}


resource "aws_key_pair" "cluster" {
  count      = var.key_name == "" ? 1 : 0
  key_name   = "${var.cluster_name}-Key"
  public_key = file("${path.module}/meios-de-pagamentos_eks_${var.environment}.pub")
  tags = {
    Name = title("${var.cluster_name}-Key")
  }
}

data "aws_subnet" "eks" {
  id = var.subnet_ids[0]
}

module "eks_cluster_tf" {
  ## Versão 17.3.0 https://github.com/terraform-aws-modules/terraform-aws-eks/tree/v17.3.0
  source                                         = "./eks_module/"
  cluster_name                                   = var.cluster_name
  cluster_version                                = var.k8s_version
  vpc_id                                         = data.aws_subnet.eks.vpc_id
  subnets                                        = var.subnet_ids
  cluster_enabled_log_types                      = var.cluster_enabled_log_types
  enable_irsa                                    = true
  cluster_endpoint_private_access_sg             = [var.compute_ec2_manager_server_k8s_sg_id]
  cluster_create_endpoint_private_access_sg_rule = var.cluster_endpoint_private_access
  cluster_endpoint_private_access                = var.cluster_endpoint_private_access
  cluster_endpoint_public_access                 = var.cluster_endpoint_public_access
  workers_role_name                              = "EKS-Workers-Role-${var.cluster_name}"
  wait_for_cluster_timeout                       = 600
  manage_aws_auth                                = var.manage_aws_auth
  workers_group_defaults = {
    instance_type           = var.instance_type
    asg_max_size            = var.asg_max_size
    asg_desired_capacity    = var.asg_desired_capacity
    asg_min_size            = var.asg_min_size
    protect_from_scale_in   = false
    key_name                = var.key_name == "" ? aws_key_pair.cluster[0].key_name : var.key_name
    root_volume_size        = var.root_volume_size
    enable_monitoring       = var.monitoring
    volume_type             = var.root_volume_type
    kubelet_extra_args      = ""
    launch_template_version = var.launch_template_version
  }

  node_groups = {
    default = {
      k8s_labels             = {}
      name                   = var.node_group_name
      create_launch_template = true
      disk_size              = var.root_volume_size
      disk_type              = var.root_volume_type
    }
  }


  map_roles    = var.roles_aws_auth
  map_users    = var.users_aws_auth
  map_accounts = var.accounts_aws_auth

  tags = {
    Name = var.cluster_name
  }
}




data "aws_iam_policy_document" "worker_policy" {
  statement {
    actions   = ["sts:AssumeRole"]
    resources = [module.eks_cluster_tf.worker_iam_role_arn]
    effect    = "Allow"
  }
}

resource "aws_iam_policy" "worker_policy" {
  name   = join("-", ["EKS-EC2-Workers-Policy", var.cluster_name])
  path   = "/"
  policy = data.aws_iam_policy_document.worker_policy.json
}

resource "aws_iam_role_policy_attachment" "worker_policy" {
  role       = module.eks_cluster_tf.worker_iam_role_name
  policy_arn = aws_iam_policy.worker_policy.arn
}
