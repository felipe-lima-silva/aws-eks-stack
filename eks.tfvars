region = "us-east-1"

application_eks_workers_instance_type           = "m5.xlarge"
application_eks_manage_aws_auth                 = true
application_eks_node_group_name                 = "prd-ng-api"
application_eks_cluster_name                    = "XXXXXXXXXXXXXX"
application_eks_cluster_enabled_log_types       = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
application_eks_k8s_version                     = "1.19"
application_eks_asg_max_size                    = 5
application_eks_asg_min_size                    = 3
application_eks_asg_desired_capacity            = 3
application_eks_workers_launch_template_version = "1"
application_eks_workers_monitoring              = true
application_eks_workers_root_volume_size        = 30
application_eks_workers_root_volume_type        = "gp3"
application_eks_cluster_endpoint_private_access = false
application_eks_cluster_endpoint_public_access  = true
subnet_ids                                      = ["XXXXXXXXX", "XXXXXXXXXXXX", "XXXXXXXXXXXXX"]
tags = {
  Env = "Producao"
}

application_eks_roles_aws_auth = [
  {
    rolearn  = "arn:aws:iam::XXXXXXXXXXXXXXX:role/EC2-Role-meios-de-pagamentos-k8s-manager"
    username = "role_tf"
    groups   = ["system:masters"]

  }
]
application_eks_users_aws_auth = [
  {
    userarn  = "arn:aws:iam::XXXXXXXXXXXXX:user/XXXXXXXXXXXXXXXX"
    username = "XXXXXXXXXXXXXX"
    groups   = ["system:masters"]
  }
]
