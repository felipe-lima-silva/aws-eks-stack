
variable "application_eks_node_group_name" {
  description = "Nome do node group."
  type        = string
}


variable "application_eks_workers_instance_type" {
  type        = string
  description = "Tipo de instânica para os workers do EKS."
}

variable "application_eks_cluster_endpoint_public_access" {

  type        = bool
  description = "Ativa o endpoint público do cluster EKS."
}

variable "application_eks_cluster_endpoint_private_access" {

  type        = bool
  description = "Ativa o endpoint privado do cluster EKS."
}

variable "subnet_ids" {
  type        = list(string)
  description = "ID das subnets para o EKS."

}

variable "application_eks_workers_launch_template_version" {
  type        = string
  description = "Versão do Launch Template dos workers do EKS."
}


variable "application_eks_cluster_enabled_log_types" {
  type        = list(string)
  default     = []
  description = "Lista com os tipos de logs que serão habilitados para o cluster EKS."
}

variable "application_eks_cluster_name" {
  type        = string
  description = "Nome do cluster EKS."
}

variable "application_eks_workers_key_name" {
  type        = string
  default     = ""
  description = "Nome da key pair para os workers do EKS."
}


variable "application_eks_k8s_version" {
  type        = string
  description = "Versão do Kubernetes."
}

variable "application_eks_manage_aws_auth" {
  type        = bool
  description = "Determina se o configmap aws-auth será gerenciado pelo Terraform. Só funciona se a máquina do Terraform tiver comunicação com o endpoint do cluster EKS."
}


variable "application_eks_asg_max_size" {
  type        = number
  description = "Capacidade máxima para o Auto Scaling Group dos workers do EKS."
}

variable "application_eks_asg_min_size" {
  type        = number
  description = "Capacidade mínima para o Auto Scaling Group dos workers do EKS."
}

variable "application_eks_asg_desired_capacity" {
  type        = number
  description = "Tamanho desejado para o Auto Scaling Group dos workers do EKS."
}


variable "application_eks_roles_aws_auth" {

  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))
  description = "Roles do IAM para interagirem com o Kubernetes. Obs: permite a visualização na console AWS dependendo do grupo escolhido."
}

variable "application_eks_users_aws_auth" {

  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  description = "Usuários do IAM para interagirem com o Kubernetes. Obs: permite a visualização na console AWS dependendo do grupo escolhido."
}

/*
variable "application_eks_cluster_additional_permissions" {
  type        = list(string)
  description = "Permissões adicionais ppara os workers do cluster EKS. Exemplo: permissões autoscaling: para o Cluster Auto Scaler."
  default     = ["autoscaling:DescribeAutoScalingGroups", "autoscaling:DescribeAutoScalingInstances", "autoscaling:DescribeLaunchConfigurations", "autoscaling:DescribeTags", "autoscaling:SetDesiredCapacity", "autoscaling:TerminateInstanceInAutoScalingGroup", "ec2:DescribeLaunchTemplateVersions"]
}
*/

variable "application_eks_accounts_aws_auth" {
  type        = list(string)
  default     = []
  description = "AWS Account IDs  para interagirem com o Kubernetes. Obs: permite a visualização na console AWS dependendo do grupo escolhido."
}


variable "application_eks_workers_monitoring" {
  type        = bool
  description = "Habilita o monitoramento detalhado para os workers do EKS."
}

variable "application_eks_workers_root_volume_size" {
  type        = number
  description = "Tamanho do volume EBS para os workers do EKS."
}

variable "application_eks_workers_root_volume_type" {
  type        = string
  description = "Tipo de volume EBS para os workers do EKS."
}

/*
variable "compute_ec2_manager_server_k8s_sg_id" {
  type        = string
  description = "ID do security group do manager server do k8s."
}
*/





