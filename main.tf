
module "eks" {
  source                               = "./modules/eks/"
  instance_type                        = var.application_eks_workers_instance_type
  node_group_name                      = var.application_eks_node_group_name
  subnet_ids                           = var.subnet_ids
  cluster_name                         = var.application_eks_cluster_name
  cluster_endpoint_public_access       = var.application_eks_cluster_endpoint_public_access
  cluster_endpoint_private_access      = var.application_eks_cluster_endpoint_private_access
  k8s_version                          = var.application_eks_k8s_version
  asg_max_size                         = var.application_eks_asg_max_size
  asg_min_size                         = var.application_eks_asg_min_size
  manage_aws_auth                      = var.application_eks_manage_aws_auth
  asg_desired_capacity                 = var.application_eks_asg_desired_capacity
  monitoring                           = var.application_eks_workers_monitoring
  key_name                             = var.application_eks_workers_key_name
  environment                          = var.environment
  roles_aws_auth                       = var.application_eks_roles_aws_auth
  users_aws_auth                       = var.application_eks_users_aws_auth
  accounts_aws_auth                    = var.application_eks_accounts_aws_auth
  root_volume_size                     = var.application_eks_workers_root_volume_size
  root_volume_type                     = var.application_eks_workers_root_volume_type
  cluster_enabled_log_types            = var.application_eks_cluster_enabled_log_types
  launch_template_version              = var.application_eks_workers_launch_template_version
  compute_ec2_manager_server_k8s_sg_id = ""
}
